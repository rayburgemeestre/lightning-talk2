<!-- $theme: gaia -->

# Lightning Talk:
<!-- *template: gaia -->

## Creating a composable class

#### With C++14 variadic templates


###### [Ray Burgemeestre](https://blog.cppse.nl/)

---
<!-- *template: gaia -->

# Candle Stick / OHLC chart


![](ohlc.png)

Green lines are a Bollinger Bands indicator

---
<!-- page_number: true -->

# Prices

```c++
#include <cmath>
#include <iostream>

int main() {
  for (time_t time = 1; time <= 1000; ++time) {
    double bid = fabs(sin(time / 30.0));
    double ask = bid + 0.005;
    std::cout << time << " - " << bid << ", " << ask
              << "\n";
  }
}
```
---
# Prices

```bash
$ g++ --std=c++14 main.cpp -o main && ./main
1 - 0.841471, 0.846471
2 - 0.909297, 0.914297
3 - 0.14112, 0.14612
4 - 0.756802, 0.761802
5 - 0.958924, 0.963924
6 - 0.279415, 0.284415
7 - 0.656987, 0.661987
8 - 0.989358, 0.994358
9 - 0.412118, 0.417118
10 - 0.544021, 0.549021
11 - 0.99999, 1.00499
12 - 0.536573, 0.541573
13 - 0.420167, 0.425167
14 - 0.990607, 0.995607
15 - 0.650288, 0.655288
16 - 0.287903, 0.292903 ...
```
---

# Desired interface 1/3

```c++
candlesticks chart;
for (...) {
  ...
  chart.add(time, bid, ask);
  if (!chart.initialized()) continue;

  // do something
  auto current_close = chart.get(0).close;
  auto previous_close = chart.get(0).close;
  auto predicted_close = chart.get(1).forecast;
  auto current_prediction = chart.get(0).forecast;
  ...
}
```
---
# Desired interface 2/3

```c++
// OHLC
chart.get(shift).open
chart.get(shift).high
chart.get(shift).low
chart.get(shift).close

// moving average
chart.get(shift).ma

// bollinger bands
chart.get(shift).upper
chart.get(shift).main
chart.get(shift).lower

// some complicated model
chart.get(shift).predicition
...
```

---
# Desired interface 3/3

```c++
candlesticks<ohlc, bollinger> chart;
...
```
---
# Step 1

```c++14
class ohlc {
public:
  double open;
  double high;
  double low;
  double close;
};

class bollinger {
public:
  double main;
  double upper;
  double lower;
};

class candlestick : public ohlc, public bollinger {};
```

---

```c++
class candlestick : public ohlc, public bollinger {};

candlestick test;
```

---

```c++
class candlestick : public ohlc, public bollinger {};

candlestick test;
```

```c++
template<class... T>
class candlestick : public T... {};

candlestick<ohlc, bollinger> test;
```
---

```c++
class candlestick : public ohlc, public bollinger {};

candlestick test;
```

```c++
template<class... T>
class candlestick : public T... {};

candlestick<ohlc, bollinger> test;
```
```c++
template<class... T>
class candlesticks {
public:
  bool initialized();
  void add(time_t time, double bid, double ask);
  candlestick<T...> get(size_t shift);
private:
  void create_candlestick_per_interval(time_t time);
  std::deque<candlestick<T...>> data_;
  time_t current_period_ = -1;
};
```
---

# initialized function
```c++ 
template<class... T>
bool candlesticks<T...>::initialized() {
  return data_.size() >= 10;
}
```
---
# get function
```c++
template <class... T>
candlestick<T...> candlesticks<T...>::get(size_t shift) {
  return data_[(data_.size() - 1) - (shift % 10)];
}
```

---
# add function

```
template <class... T>
void candlesticks<T...>::add(time_t time,
                             double bid,
                             double ask) {
  // ???
}
```
```c++14
class ohlc {
public:
  double open;
  double high;
  double low;
  double close;
};
```

---
# add function

```
template <class... T>
void candlesticks<T...>::add(time_t time,
                             double bid,
                             double ask) {
  // call update here on all base classes
}
```
```c++14
class ohlc {
public:
  double open;
  double high;
  double low;
  double close;
  
  void update(double bid, double ask);
};
```
---
# ohlc update

```c++
class ohlc {
public:
  double open = 0;
  double high = std::numeric_limits<double>::min();
  double low  = std::numeric_limits<double>::max();
  double close = 0;

  void update(double bid, double ask);
};

void ohlc::update(double bid, double ask) {
  if (open == 0) open = bid;
  high = std::max(bid, high);
  low = std::min(bid, low);
  close = bid;
}
```

---
# bollinger update
```c++
class bollinger {
public:
  double main;
  double upper;
  double lower;

  void update(double bid, double ask);
};

void bollinger::update(double bid, double ask) {
  // TODO:
  // main = moving average past 10 close prices
  // upper = main +  2 times standard deviation
  // lower = main -  2 times standard deviation
}
```

---
# add function

```c++
template <class... T>
void candlesticks<T...>::add(time_t time,
                             double bid,
                             double ask) {
  create_candlestick_per_interval(time);
  data_.back().update(bid, ask);  // ambiguous!
}
```
---
# add function

```c++
template <class... T>
void candlesticks<T...>::add(time_t time,
                             double bid,
                             double ask) {
  create_candlestick_per_interval(time);
  data_.back().update(bid, ask);  // ambiguous!
}
template <class... T>
void candlesticks<T...>::create_candlestick_per_interval(
    time_t time) {
  time_t period = time / 15;
  if (current_period_ != period) {
    data_.push_back({});
    if (data_.size() > 10) {
      data_.pop_front();
    }
    current_period_ = period;
  }
}
```

---
# candlestick update function

```c++
template<class... T> class candlestick : public T... {
public:
  void update(double bid, double ask)
  {
      // ???
  }
};
```
---
# candlestick update function

```c++
template<class... T> class candlestick : public T... {
public:
  void update(double bid, double ask)
  {
    update<T...>(bid, ask);
  }

  template <class head, class... tail>
  void update(double bid, double ask)
  {
    static_cast<head *>(this)->update(bid, ask);
    update<tail...>(bid, ask);
  }
};
```
---
```
main.cpp: In instantiation of ‘void candlestick<T>::update(double, double) [with head = bollinger; tail = {}; T = {ohlc, bollinger}]’:
main.cpp:59:20:   required from ‘void candlestick<T>::update(double, double) [with head = ohlc; tail = {bollinger}; T = {ohlc, bollinger}]’
main.cpp:46:17:   required from ‘void candlestick<T>::update(double, double) [with T = {ohlc, bollinger}]’
main.cpp:90:3:   required from ‘void candlesticks<T>::add(time_t, double, double) [with T = {ohlc, bollinger}; time_t = long int]’
main.cpp:99:29:   required from here
main.cpp:59:20: error: no matching function for call to ‘candlestick<ohlc, bollinger>::update(double&, double&)’
     update<tail...>(bid, ask);
     ~~~~~~~~~~~~~~~^~~~~~~~~~
main.cpp:56:8: note: candidate: template<class head, class ... tail> void candlestick<T>::update(double, double) [with head = head; tail = {tail ...}; T = {ohlc, bollinger}]
   void update(double bid, double ask)
        ^~~~~~
main.cpp:56:8: note:   template argument deduction/substitution failed:
main.cpp:59:20: note:   couldn't deduce template parameter ‘head’
     update<tail...>(bid, ask);
     ~~~~~~~~~~~~~~~^~~~~~~~~~
```
---

```c++
template<class... T> class candlestick : public T... {
public:
  void update(double bid, double ask)
  {
    update<T...>(bid, ask);
  }
  
  template <class head>
  void update (double bid, double ask) {
    static_cast<head *>(this)->update(bid, ask);
  }

  template <class head, class... tail>
  void update(double bid, double ask)
  {
    static_cast<head *>(this)->update(bid, ask);
    update<tail...>(bid, ask);
  }
};
```
---
```
main.cpp: In instantiation of ‘void candlestick<T>::update(double, double) [with head = ohlc; tail = {bollinger}; T = {ohlc, bollinger}]’:
main.cpp:46:17:   required from ‘void candlestick<T>::update(double, double) [with T = {ohlc, bollinger}]’
main.cpp:90:3:   required from ‘void candlesticks<T>::add(time_t, double, double) [with T = {ohlc, bollinger}; time_t = long int]’
main.cpp:99:29:   required from here
main.cpp:59:20: error: call of overloaded ‘update(double&, double&)’ is ambiguous
     update<tail...>(bid, ask);
     ~~~~~~~~~~~~~~~^~~~~~~~~~
main.cpp:50:8: note: candidate: void candlestick<T>::update(double, double) [with head = bollinger; T = {ohlc, bollinger}]
   void update (double bid, double ask) {
        ^~~~~~
main.cpp:56:8: note: candidate: void candlestick<T>::update(double, double) [with head = bollinger; tail = {}; T = {ohlc, bollinger}]
   void update(double bid, double ask)
        ^~~~~~
```
---
```c++
template <class... T>
class candlestick : public T... {
public:
  void update(double bid, double ask) {
    update<T...>(bid, ask);
  }

  template <class head>
  void update(double bid, double ask) {
    static_cast<head *>(this)->update(bid, ask);
  }

  template <class head, class... tail>
  typename std::enable_if<0 != sizeof...(tail)>::type
  update(double bid, double ask) {
    static_cast<head *>(this)->update(bid, ask);
    update<tail...>(bid, ask);
  }
};
```
---

# The result

```
$ g++ --std=c++14 main.cpp -o main && ./main 
268 Buy at: 0.4769
308 Close at: 0.745902
```
```c++
if (chart.get(1).close > 0.75 &&
    chart.get(0).close < 0.50 && !in_trade
){
  std::cout << time << " Buy at: " << ask << "\n";
  in_trade = true;
} else if (ask > 0.75 && in_trade) {
  std::cout << time << " Close at: " << bid << "\n";
  break;
}
```

---

#  Resources

![left](c++templates.png) ![right](Selection_001.bmp)
* Book: 12.4.2 - Where Can Pack Expansions Occur?
* [C++Now 2018: Odin Holmes “C++ Mixins: Customization Through Compile Time Composition”](https://www.youtube.com/watch?v=wWZi_wPyVvs)

---

# Link to source code

* https://bitbucket.org/rayburgemeestre/lightning-talk2/src

