Slides are in `presentation` sub-directory.

Lightning talk given here: https://www.meetup.com/The-Dutch-Cpp-Group/events/252743396/
EST: +/- 12 minutes

For rendering and running the presentation, I've merged a few different pull-requests to get the features I wanted in this branch:
- https://github.com/rayburgemeestre/marp/tree/add-presentation-mode-rebased

Compiling: g++ main.cpp -o main

Running: ./main

