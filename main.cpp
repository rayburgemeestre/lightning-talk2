#include <algorithm>
#include <cmath>
#include <deque>
#include <iostream>
#include <limits>
#include <numeric>

class ohlc {
public:
  double open = 0;
  double high = std::numeric_limits<double>::min();
  double low = std::numeric_limits<double>::max();
  double close = 0;

  template <typename deque>
  void update(const deque &data, double bid, double ask);
};

template <typename deque>
void ohlc::update(const deque &data,
                  double bid,
                  double ask) {
  if (open == 0) {
    open = bid;
  }
  high = std::max(bid, high);
  low = std::min(bid, low);
  close = bid;
}

class bollinger {
public:
  double main;
  double upper;
  double lower;

  template <typename deque>
  void update(const deque &data, double bid, double ask);
};

template <typename deque>
void bollinger::update(const deque &data,
                       double bid,
                       double ask) {
  main = std::accumulate(data.begin(),
                         data.end(),
                         0.0,
                         [](double sum, auto candle) {
                           return sum + candle.close;
                         }) /
         data.size();
  auto stddev =
      sqrt(std::accumulate(
               data.begin(),
               data.end(),
               0.0,
               [&](double sum_diff_squared, auto candle) {
                 return sum_diff_squared +
                        pow(main - candle.close, 2);
               }) /
           data.size());
  lower = main - (2.0 * stddev);
  upper = main + (2.0 * stddev);
}

template <class... T>
class candlestick : public T... {
public:
  template <typename deque>
  void update(const deque &data, double bid, double ask) {
    update<deque, T...>(data, bid, ask);
  }

  template <typename deque, class head>
  void update(const deque &data, double bid, double ask) {
    static_cast<head *>(this)->update(data, bid, ask);
  }

  template <typename deque, class head, class... tail>
  typename std::enable_if<0 != sizeof...(tail)>::type
  update(const deque &data, double bid, double ask) {
    static_cast<head *>(this)->update(data, bid, ask);
    update<deque, tail...>(data, bid, ask);
  }
};

template <class... T>
class candlesticks {
public:
  bool initialized();
  void add(time_t time, double bid, double ask);
  candlestick<T...> get(size_t shift);

private:
  void create_candlestick_per_interval(time_t time);
  std::deque<candlestick<T...>> data_;
  time_t current_period_ = -1;
};

template <class... T>
bool candlesticks<T...>::initialized() {
  return data_.size() >= 10;
}
template <class... T>
void candlesticks<T...>::add(time_t time,
                             double bid,
                             double ask) {
  create_candlestick_per_interval(time);
  data_.back().update(data_, bid, ask);
}

template <class... T>
void candlesticks<T...>::create_candlestick_per_interval(
    time_t time) {
  time_t period = time / 15;
  if (current_period_ != period) {
    data_.push_back({});
    if (data_.size() > 10) {
      data_.pop_front();
    }
    current_period_ = period;
  }
}

template <class... T>
candlestick<T...> candlesticks<T...>::get(size_t shift) {
  return data_[data_.size() - 1 - (shift % 10)];
}

int main() {
  candlesticks<ohlc, bollinger> chart;
  bool in_trade = false;
  for (time_t time = 1; time <= 1000; ++time) {
    double bid = fabs(sin(time / 30.0));
    double ask = bid + 0.005;
    chart.add(time, bid, ask);

    if (!chart.initialized()) continue;

    if (chart.get(1).close > 0.75 &&
        chart.get(0).close < 0.50 && !in_trade) {
      std::cout << time << " Buy at: " << ask << "\n";
      in_trade = true;
    } else if (ask > 0.75 && in_trade) {
      std::cout << time << " Close at: " << bid << "\n";
      break;
    }
  }
}
